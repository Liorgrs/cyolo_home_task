import React from 'react'
import ServerView from './components/ServerView'
import cyoloServer from './store/Server'

function App() {
  return (
    <div>
      Server view component below
      <ServerView server={cyoloServer} />
    </div>
  )
}

export default App

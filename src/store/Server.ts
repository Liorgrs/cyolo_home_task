import { makeAutoObservable } from 'mobx'

export class CyoloServer {
  connected: boolean

  constructor() {
    makeAutoObservable(this)
    this.connected = false
  }
  
  async connect() {
    // TODO: Think why this is not calling the server
    try {
      const response = await fetch('localhost:3001/')
      if (response) {
        this.connected = true
      }
    } catch (error) {}
  }
}

const cyoloServer = new CyoloServer()

export default cyoloServer

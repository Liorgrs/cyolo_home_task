import React, { FC, useEffect } from 'react'
import { CyoloServer } from '../store/Server'
import { observer } from 'mobx-react-lite'

interface ServerViewProps {
  server: InstanceType<typeof CyoloServer>
}

const ServerView: FC<ServerViewProps> = observer(({ server }) => {
  useEffect(() => {
    server.connect()
  }, [server])

  return <div>Am i connected? {server.connected ? 'true' : 'false'}</div>
})

export default ServerView
